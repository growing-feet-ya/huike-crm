package com.huike.review.mapper;

import com.huike.common.core.domain.AjaxResult;
import com.huike.review.pojo.Review;
import com.huike.review.vo.MybatisReviewVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * Mybatis复习的Mapper层
 */
public interface MybatisReviewMapper {



    /**======================================================新增======================================================**/
    void saveData(@Param("name") String name,@Param("sex") String sex,@Param("age") String age);

    void saveData(MybatisReviewVO review);




    /**======================================================删除======================================================**/

    /**======================================================修改======================================================**/

    /**======================================================简单查询===================================================**/
    Review getById(@Param("id") Long id);

    List<Review> getTableDataInfo();
}
