package com.huike.review.service;

import com.huike.common.core.domain.AjaxResult;
import com.huike.review.pojo.Review;
import com.huike.review.vo.MybatisReviewVO;

import java.util.List;

/**
 * mybatis复习接口层
 */
public interface ReviewService {


    AjaxResult saveData(String name, String sex, String age);

    AjaxResult saveData(MybatisReviewVO review);

    AjaxResult getById(Long id);

    List getTableDataInfo();
}
