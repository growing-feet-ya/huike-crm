package com.huike.web.controller.review;


import com.huike.common.core.controller.BaseController;
import com.huike.common.core.domain.AjaxResult;
import com.huike.common.core.page.TableDataInfo;
import com.huike.review.pojo.Review;
import com.huike.review.service.ReviewService;
import com.huike.review.vo.MybatisReviewVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 该Controller主要是为了复习三层架构以及Mybatis使用的，该部分接口已经放开权限，可以直接访问
 * 同学们在此处编写接口通过浏览器访问看是否能完成最简单的增删改查
 */
@RestController
@RequestMapping("/review")
public class MybatisReviewController extends BaseController {

    @Autowired
    private ReviewService reviewService;

    /**=========================================================新增数据============================================*/
    @GetMapping("/saveData/{name}/{age}/{sex}")
    public AjaxResult saveData1(@PathVariable("name")String name,
                                @PathVariable("age")String age,
                                @PathVariable("sex")String sex){
        return reviewService.saveData(name, age, sex);
    }

    @PostMapping("/saveData")
    public AjaxResult saveDate(@RequestBody MybatisReviewVO reviewVO){
        return reviewService.saveData(reviewVO);
    }
    /**=========================================================删除数据=============================================*/



    /**=========================================================修改数据=============================================*/

    /**=========================================================查询数据=============================================*/
    @GetMapping("/getById")
    public AjaxResult getDataById(@RequestParam("id")Long id){
        return reviewService.getById(id);
    }

    @GetMapping("/getDataByPage")
    public TableDataInfo getDataByPage(@RequestParam("pageNum")Integer pageNum,@RequestParam("pageSize") Integer pageSize){
        startPage();
        List result = reviewService.getTableDataInfo();
        return getDataTable(result);

    }

}