package com.huike;

import com.huike.report.domain.vo.DragonTigerVO;
import com.huike.report.domain.vo.MyResultVo;
import com.huike.report.mapper.ReportMapper;
import com.huike.report.service.impl.ReportServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Author 12441
 * @Date 2022/8/28 20:37
 * @Version 1.0
 */
@SpringBootTest
public class HuiKeApplicationTest {
    @Autowired
    ReportMapper reportMapper;
    @Autowired
    ReportServiceImpl reportServiceImpl;
    @Test
    public void myTest1(){
        List<MyResultVo> resultVo = reportMapper.getEmployeeRankInfo(null,null);
        System.out.println(resultVo);
    }
    @Test void mytest2(){

        //1）构建一个空的结果集对象
        List<DragonTigerVO> result;
        //2 封装结果集属性
        result = reportMapper.getEmployeeRankInfo(null, null).stream().map((item) -> {

            DragonTigerVO dragonTigerVO = new DragonTigerVO();

                //item -MyResult对象
                //3 封装结果集对象
                dragonTigerVO.setCreate_by(item.getUserName());
                System.out.println(">>>>>>>>>>>>>>>>>>>>>"+item.getUserName());
                dragonTigerVO.setDeptName(item.getDeptName());
                System.out.println(">>>>>>>>>>>>>>>>>>>>>"+item.getDeptName());
                dragonTigerVO.setNum(Math.toIntExact(item.getNum()));
                System.out.println(">>>>>>>>>>>>>>>>>>>>>"+Math.toIntExact(item.getNum()));

                //计算转换率
                Integer allNumByUser = reportMapper.getContractAllNumByUser(item.getUserName());
                BigDecimal radio = reportServiceImpl.getRadio(allNumByUser, item.getNum());
                dragonTigerVO.setRadio(radio);
                System.out.println(">>>>>>>>>>>>>>>>>>>>>"+radio);

            return dragonTigerVO;
        }).collect(Collectors.toList());
        //4 返回结果集对象
        System.out.println(result);

    }
    @Test
    public void test3(){
        Integer clueCountByStatus = reportMapper.getClueCountByStatus(2);
        System.out.println(clueCountByStatus);

    }
}
