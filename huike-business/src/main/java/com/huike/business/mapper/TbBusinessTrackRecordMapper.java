package com.huike.business.mapper;

import java.util.List;
import com.huike.business.domain.TbBusinessTrackRecord;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商机跟进记录Mapper接口
 * @date 2021-04-28
 */
@Mapper
public interface TbBusinessTrackRecordMapper {

}