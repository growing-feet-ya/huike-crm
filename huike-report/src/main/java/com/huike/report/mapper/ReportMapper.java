package com.huike.report.mapper;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.huike.report.domain.vo.IndexTodoInfoVO;
import com.huike.report.domain.vo.MyResult2Vo;
import com.huike.report.domain.vo.MyResultVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.huike.clues.domain.vo.IndexStatisticsVo;

/**
 * 首页统计分析的Mapper
 * @author Administrator
 *
 */
@Mapper
public interface ReportMapper {
	/**=========================================基本数据========================================*/
	/**
	 * 获取线索数量
	 * @param beginCreateTime	开始时间
	 * @param endCreateTime		结束时间
	 * @param username			用户名
	 * @return
	 */
	Integer getCluesNum(@Param("startTime") String beginCreateTime,
						@Param("endTime") String endCreateTime,
						@Param("username") String username);

	/**
	 * 获取商机数量
	 * @param beginCreateTime	开始时间
	 * @param endCreateTime		结束时间
	 * @param username			用户名
	 * @return
	 */
	Integer getBusinessNum(@Param("startTime") String beginCreateTime,
						   @Param("endTime") String endCreateTime,
						   @Param("username") String username);

	/**
	 * 获取合同数量
	 * @param beginCreateTime	开始时间
	 * @param endCreateTime		结束时间
	 * @param username			用户名
	 * @return
	 */
	Integer getContractNum(@Param("startTime") String beginCreateTime,
						   @Param("endTime") String endCreateTime,
						   @Param("username") String username);

	/**
	 * 获取合同金额
	 * @param beginCreateTime	开始时间
	 * @param endCreateTime		结束时间
	 * @param username			用户名
	 * @return
	 */
	Double getSalesAmount(@Param("startTime") String beginCreateTime,
						  @Param("endTime") String endCreateTime,
						  @Param("username") String username);

	/**=========================================今日简报========================================*/
	/**
	 * 获取线索数量
	 * @param username			用户名
	 * @return
	 */
	Integer getCluesNum1(@Param("username") String username);
	/**
	 * 获取商机数量
	 * @param username			用户名
	 * @return
	 */
	Integer getBusinessNum1(@Param("username") String username);

	/**
	 * 获取合同数量
	 * @param username			用户名
	 * @return
	 */
	Integer getContractNum1(@Param("username") String username);

	/**
	 * 获取合同金额
	 *@param username			用户名
	 * @return
	 */
	Double getSalesAmount1(@Param("username") String username);

	/**=========================================Vulnerability========================================*/

	Integer getCluesNumForVulnerability(@Param("startTime") String beginCreateTime,
										@Param("endTime") String endCreateTime);

    Integer getEffectiveCluesNums(@Param("startTime") String beginCreateTime,
								  @Param("endTime") String endCreateTime);

	Integer getBusinessNumsForVulnerability(@Param("startTime") String beginCreateTime,
											@Param("endTime") String endCreateTime);

	Integer getContractNumForVulnerability(@Param("startTime") String beginCreateTime,
										   @Param("endTime") String endCreateTime);
	/**=========================================商机龙虎榜========================================*/
	List<MyResultVo> getEmployeeRankInfo(@Param("startTime") String beginCreateTime,
										 @Param("endTime") String endCreateTime);
	Integer getContractAllNumByUser(@Param("userName") String userName);

	/**=========================================线索龙虎榜========================================*/
	List<MyResultVo> getEmployeeRankInfoTwo(@Param("startTime")String beginCreateTime, @Param("endTime")String endCreateTime);
	Integer getBusinessAllNumByUser(@Param("userName") String userName);

	Integer getClueCountByStatus(@Param("status") Integer status);

	Integer getBusinessCountByStatus(@Param("status") Integer status);
	/**=========================================查询合同学科数量和姓名========================================*/
	List<MyResult2Vo> selectContractNumAndName(@Param("startTime")String beginCreateTime, @Param("endTime")String endCreateTime);

	/**=========================================待办========================================*/


}
