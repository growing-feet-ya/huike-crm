package com.huike.report.domain.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * 今日简报数据
 */
@Data
public class DragonTigerVO {
    private String create_by;  //用户名称
    private String deptName ;  //部门名称
    private Integer num=0;  //转化数量
    private BigDecimal radio;  //转化率
}
