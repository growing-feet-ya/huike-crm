package com.huike.report.domain.vo;

import lombok.Data;

/**
 * @Author 12441
 * @Date 2022/8/28 20:32
 * @Version 1.0
 */
@Data
public class MyResult2Vo {
    private String subject;//学科
    private Long num;//合同数量
}

