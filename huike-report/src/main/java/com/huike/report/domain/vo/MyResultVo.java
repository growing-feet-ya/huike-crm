package com.huike.report.domain.vo;

import lombok.Data;

/**
 * @Author 12441
 * @Date 2022/8/28 20:32
 * @Version 1.0
 */
@Data
public class MyResultVo {
    private Long num;
    private String userName;
    private String deptName;
}
